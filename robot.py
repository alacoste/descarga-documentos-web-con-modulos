import urllib.request as urlRequest
import os

class Robot:
    def __init__(self, url):
        self.url = url

    # .. Asigno como nombre del documento el ultimo elemento de la url
    def getFname(self):
        filename = self.url.split("/")[-1]
        if filename == '':
            filename = self.url.split("/")[-2]
        return filename

    def retrieve(self):
        # .. Descargar datos/documentos de la url
        filename = self.getFname()
        if not os.path.exists(filename):
            endpoint = urlRequest.urlopen(self.url)
            doc = endpoint.read().decode('utf-8')
            file = open(filename, "w")
            file.write(doc)
            file.close()
            print("Descargando Url")

    def show(self):
        self.retrieve()
        file = self.getFname()
        file = open(file, "r")
        print(file.read())
        file.close()

    def content(self):
        fname = self.getFname()
        doc = "No hay contenido para esa url, descarguela primero"
        if os.path.exists(fname):
            file = open(fname, "r")
            doc = file.read()
            file.close()
        return doc