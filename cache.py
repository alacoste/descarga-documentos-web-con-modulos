import robot

class Cache:
    robots = []

    # Esta función devuelve el índice del robot cuyo url coincide con el parámetro url del robot
    def getRobot(self, url):
        for i in range(0, len(self.robots), 1):
            if self.robots[i].url == url:
                return i
        return None

    def retrieve(self, url):
        if self.getRobot(url) == None:
            self.robots.append(robot.Robot(url))
        self.robots[self.getRobot(url)].retrieve()

    def show(self, url):
        self.retrieve(url)
        file = self.robots[self.getRobot(url)].getFname()
        file = open(file, "r")
        print(file.read())
        file.close()

    def show_all(self):
        print("Listado de Urls descargados:")
        for robot in self.robots:
            print(robot.url)

    def content(self, url):
        self.retrieve(url)
        return self.robots[self.getRobot(url)].content()