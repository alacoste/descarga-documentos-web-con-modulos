#!/usr/bin/python3
import robot
import cache

if __name__ == '__main__':
    try:
        robot1 = robot.Robot("https://www.youtube.com")
        robot2 = robot.Robot("https://www.urjc.es")

        robot1.retrieve()
        robot1.show()
        print(robot1.content)

        robot2.retrieve()
        robot2.show()
        print(robot2.content)

        cache1 = cache.Cache()
        cache2 = cache.Cache()

        cache1.retrieve("https://www.urjc.es/intranet-urjc")
        cache1.retrieve("https://cursosweb.github.io/")
        print(cache1.content("https://www.youtube.com"))
        cache1.show("https://www.youtube.com")
        cache1.show_all()

        cache2.retrieve("https://www.3djuegos.com/")
        cache2.show("https://www.3djuegos.com/")
        print(cache2.content("https://www.youtube.com"))
        cache2.show_all()
    except UnicodeDecodeError:
        print("Lo siento pero solo admitimos decodificaciones utf-8")
